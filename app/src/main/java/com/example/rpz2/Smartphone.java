package com.example.rpz2;


public class Smartphone {
  private int id;
  private String manufacturer;
  private String model;
  private double screenSize;
  private String warehouseAddress;
  private double price;

  public Smartphone() {
  }

  public Smartphone(int id, String manufacturer, String model, double screenSize, String warehouseAddress, double price) {
    this.id = id;
    this.manufacturer = manufacturer;
    this.model = model;
    this.screenSize = screenSize;
    this.warehouseAddress = warehouseAddress;
    this.price = price;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getManufacturer() {
    return manufacturer;
  }

  public void setManufacturer(String manufacturer) {
    this.manufacturer = manufacturer;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public double getScreenSize() {
    return screenSize;
  }

  public void setScreenSize(double screenSize) {
    this.screenSize = screenSize;
  }

  public String getWarehouseAddress() {
    return warehouseAddress;
  }

  public void setWarehouseAddress(String warehouseAddress) {
    this.warehouseAddress = warehouseAddress;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }
}
