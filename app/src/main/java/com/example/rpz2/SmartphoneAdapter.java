package com.example.rpz2;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class SmartphoneAdapter extends RecyclerView.Adapter<SmartphoneAdapter.ViewHolder> {

  private List<Smartphone> smartphones;
  public SmartphoneAdapter(List<Smartphone> smartphones) {
    this.smartphones = smartphones;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_smartphone, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    Smartphone smartphone = smartphones.get(position);

    holder.textManufacturer.setText("Manufacturer: " + smartphone.getManufacturer());
    holder.textModel.setText("Model: " + smartphone.getModel());
    holder.textScreenSize.setText("Screen Size: " + smartphone.getScreenSize());
    holder.textWarehouseAddress.setText("Warehouse Address: " + smartphone.getWarehouseAddress());
    holder.textPrice.setText("Price: $" + smartphone.getPrice());
    holder.itemView.setOnClickListener(view -> {
      // Передайте адресу складу у нову Activity з картою
      Intent intent = new Intent(view.getContext(), MapActivity.class);
      intent.putExtra("warehouse_address", smartphone.getWarehouseAddress());
      view.getContext().startActivity(intent);
    });
  }

  @Override
  public int getItemCount() {
    return smartphones.size();
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {
    TextView textManufacturer;
    TextView textModel;
    TextView textScreenSize;
    TextView textWarehouseAddress;
    TextView textPrice;


    public ViewHolder(@NonNull View itemView) {
      super(itemView);

      textManufacturer = itemView.findViewById(R.id.textManufacturer);
      textModel = itemView.findViewById(R.id.textModel);
      textScreenSize = itemView.findViewById(R.id.textScreenSize);
      textWarehouseAddress = itemView.findViewById(R.id.textWarehouseAddress);
      textPrice = itemView.findViewById(R.id.textPrice);
    }
  }
}
