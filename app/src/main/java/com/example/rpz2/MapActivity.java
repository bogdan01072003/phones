package com.example.rpz2;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import java.io.IOException;
import java.util.List;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

  private GoogleMap mMap;
  private String warehouseAddress;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_map);

    // Отримайте адресу складу з Intent
    warehouseAddress = getIntent().getStringExtra("warehouse_address");

  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    mMap = googleMap;

    if (warehouseAddress != null) {
      Geocoder geocoder = new Geocoder(this);
      List<Address> addresses;
      try {
        addresses = geocoder.getFromLocationName(warehouseAddress, 1);
        if (!addresses.isEmpty()) {
          Address address = addresses.get(0);
          LatLng warehouseLocation = new LatLng(address.getLatitude(), address.getLongitude());
          mMap.addMarker(new MarkerOptions().position(warehouseLocation).title("Warehouse"));
          mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(warehouseLocation, 15f));
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
