package com.example.rpz2;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

  private static final int DATABASE_VERSION = 1;
  private static final String DATABASE_NAME = "SmartphonesDB";
  private static final String TABLE_NAME = "smartphones";
  private static final String COLUMN_ID = "id";
  private static final String COLUMN_MANUFACTURER = "manufacturer";
  private static final String COLUMN_MODEL = "model";
  private static final String COLUMN_SCREEN_SIZE = "screen_size";
  private static final String COLUMN_WAREHOUSE_ADDRESS = "warehouse_address";
  private static final String COLUMN_PRICE = "price";

  public DatabaseHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    String CREATE_SMARTPHONES_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
        COLUMN_ID + " INTEGER PRIMARY KEY, " +
        COLUMN_MANUFACTURER + " TEXT, " +
        COLUMN_MODEL + " TEXT, " +
        COLUMN_SCREEN_SIZE + " REAL, " +
        COLUMN_WAREHOUSE_ADDRESS + " TEXT, " +
        COLUMN_PRICE + " REAL)";
    db.execSQL(CREATE_SMARTPHONES_TABLE);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    onCreate(db);
  }

  public void addSmartphone(Smartphone smartphone) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues values = new ContentValues();
    values.put(COLUMN_MANUFACTURER, smartphone.getManufacturer());
    values.put(COLUMN_MODEL, smartphone.getModel());
    values.put(COLUMN_SCREEN_SIZE, smartphone.getScreenSize());
    values.put(COLUMN_WAREHOUSE_ADDRESS, smartphone.getWarehouseAddress());
    values.put(COLUMN_PRICE, smartphone.getPrice());
    db.insert(TABLE_NAME, null, values);
    db.close();
  }


  public List<Smartphone> getAllSmartphones() {
    List<Smartphone> smartphones = new ArrayList<>();
    String selectQuery = "SELECT * FROM " + TABLE_NAME;
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.rawQuery(selectQuery, null);
    if (cursor.moveToFirst()) {
      do {
        Smartphone smartphone = new Smartphone();
        smartphone.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
        smartphone.setManufacturer(cursor.getString(cursor.getColumnIndex(COLUMN_MANUFACTURER)));
        smartphone.setModel(cursor.getString(cursor.getColumnIndex(COLUMN_MODEL)));
        smartphone.setScreenSize(cursor.getDouble(cursor.getColumnIndex(COLUMN_SCREEN_SIZE)));
        smartphone.setWarehouseAddress(cursor.getString(cursor.getColumnIndex(COLUMN_WAREHOUSE_ADDRESS)));
        smartphone.setPrice(cursor.getDouble(cursor.getColumnIndex(COLUMN_PRICE)));
        smartphones.add(smartphone);
      } while (cursor.moveToNext());
    }
    cursor.close();
    db.close();
    return smartphones;
  }

  public List<Smartphone> getMotorolaPhonesWithScreenSizeGreaterThan(double minScreenSize) {
    List<Smartphone> motorolaPhones = new ArrayList<>();
    String selectQuery = "SELECT * FROM " + TABLE_NAME +
        " WHERE " + COLUMN_MANUFACTURER + "='Motorola' AND " +
        COLUMN_SCREEN_SIZE + ">" + minScreenSize;
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.rawQuery(selectQuery, null);
    if (cursor.moveToFirst()) {
      do {
        Smartphone smartphone = new Smartphone();
        smartphone.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
        smartphone.setManufacturer(cursor.getString(cursor.getColumnIndex(COLUMN_MANUFACTURER)));
        smartphone.setModel(cursor.getString(cursor.getColumnIndex(COLUMN_MODEL)));
        smartphone.setScreenSize(cursor.getDouble(cursor.getColumnIndex(COLUMN_SCREEN_SIZE)));
        smartphone.setWarehouseAddress(cursor.getString(cursor.getColumnIndex(COLUMN_WAREHOUSE_ADDRESS)));
        smartphone.setPrice(cursor.getDouble(cursor.getColumnIndex(COLUMN_PRICE)));
        motorolaPhones.add(smartphone);
      } while (cursor.moveToNext());
    }
    cursor.close();
    db.close();
    return motorolaPhones;
  }


  public double getAverageScreenSize() {
    double averageScreenSize = 0;
    String selectQuery = "SELECT AVG(" + COLUMN_SCREEN_SIZE + ") FROM " + TABLE_NAME;
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.rawQuery(selectQuery, null);
    if (cursor.moveToFirst()) {
      averageScreenSize = cursor.getDouble(0);
    }
    cursor.close();
    db.close();
    return averageScreenSize;
  }




  // Додайте інші методи за потреби
}

