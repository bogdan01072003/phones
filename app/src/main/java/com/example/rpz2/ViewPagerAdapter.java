package com.example.rpz2;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class ViewPagerAdapter extends FragmentStateAdapter {

  private final DatabaseHelper db;

  public ViewPagerAdapter(@NonNull FragmentActivity fragmentActivity, DatabaseHelper db) {
    super(fragmentActivity);
    this.db = db;
  }

  @NonNull
  @Override
  public Fragment createFragment(int position) {
    if (position == 0) {
      return new AllPhonesFragment(db.getAllSmartphones());
    } else {
      return new MotorolaPhonesFragment(db.getMotorolaPhonesWithScreenSizeGreaterThan(5.0));
    }
  }

  @Override
  public int getItemCount() {
    return 2; // дві вкладки (All та Motorola)
  }
}
