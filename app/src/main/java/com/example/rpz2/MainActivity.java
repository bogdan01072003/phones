package com.example.rpz2;
import android.os.Bundle;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;


public class MainActivity extends AppCompatActivity {

  private DatabaseHelper db;
  private TextView averageScreenSizeTextView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    db = new DatabaseHelper(this);
    //addTestData();
    ViewPager2 viewPager = findViewById(R.id.viewPager);
    viewPager.setAdapter(new ViewPagerAdapter(this, db));

    TabLayout tabLayout = findViewById(R.id.tabLayout);
    new TabLayoutMediator(tabLayout, viewPager,
        (tab, position) -> tab.setText(position == 0 ? "All" : "Motorola"))
        .attach();
    averageScreenSizeTextView = findViewById(R.id.averageScreenSizeTextView);
    double averageScreenSize = db.getAverageScreenSize();
    averageScreenSizeTextView.setText("Average Screen Size: " + averageScreenSize);

  }

  // Функція для додавання тестових даних (можна видалити пізніше)

  private void addTestData() {
    Smartphone smartphone1 = new Smartphone(1, "Apple", "iPhone 13", 6.1, "Address 1", 999.99);
    Smartphone smartphone2 = new Smartphone(2, "Samsung", "Galaxy S21", 6.4, "Address 2", 899.99);
    Smartphone smartphone3 = new Smartphone(3, "Google", "Pixel 6", 6.0, "Address 3", 799.99);
    Smartphone smartphone4 = new Smartphone(4, "Motorola", "Moto G Power", 6.6, "Address 4", 299.99);
    Smartphone smartphone5 = new Smartphone(5, "Xiaomi", "Redmi Note 10", 6.43, "Address 5", 249.99);
    Smartphone smartphone6 = new Smartphone(6, "OnePlus", "OnePlus 9", 6.55, "Address 6", 799.99);
    Smartphone smartphone7 = new Smartphone(7, "Huawei", "P40 Pro", 6.58, "Address 7", 899.99);
    Smartphone smartphone8 = new Smartphone(8, "Sony", "Xperia 5 II", 6.1, "Address 8", 999.99);
    Smartphone smartphone9 = new Smartphone(9, "Motorola", "Moto Edge", 6.7, "Address 9", 599.99);
    Smartphone smartphone10 = new Smartphone(10, "Realme", "Realme 8 Pro", 6.4, "Address 10", 299.99);
    Smartphone smartphone11 = new Smartphone(11, "Motorola", "Moto G Stylus", 6.8, "Address 11", 399.99);
    Smartphone smartphone12 = new Smartphone(12, "Oppo", "Oppo Reno 6", 6.43, "Address 12", 499.99);
    Smartphone smartphone13 = new Smartphone(13, "LG", "LG Velvet", 6.8, "Address 13", 699.99);
    Smartphone smartphone14 = new Smartphone(14, "Motorola", "Moto G9 Plus", 6.81, "Address 14", 349.99);
    Smartphone smartphone15 = new Smartphone(15, "Nokia", "Nokia 8.3", 6.81, "Address 15", 599.99);

// Додавання смартфонів до бази даних
    db.addSmartphone(smartphone1);
    db.addSmartphone(smartphone2);
    db.addSmartphone(smartphone3);
    db.addSmartphone(smartphone4);
    db.addSmartphone(smartphone5);
    db.addSmartphone(smartphone6);
    db.addSmartphone(smartphone7);
    db.addSmartphone(smartphone8);
    db.addSmartphone(smartphone9);
    db.addSmartphone(smartphone10);
    db.addSmartphone(smartphone11);
    db.addSmartphone(smartphone12);
    db.addSmartphone(smartphone13);
    db.addSmartphone(smartphone14);
    db.addSmartphone(smartphone15);
  }

}
