package com.example.rpz2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

public class AllPhonesFragment extends Fragment {

  private final List<Smartphone> allPhones;

  public AllPhonesFragment(List<Smartphone> allPhones) {
    this.allPhones = allPhones;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_phone_list, container, false);
    RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
    recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
    SmartphoneAdapter adapter = new SmartphoneAdapter(allPhones);
    recyclerView.setAdapter(adapter);
    return view;
  }
}
